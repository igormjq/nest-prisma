import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { UserModule } from './user/user.module';
import { PrismaModule } from './prisma/prisma.module';
import { AnimalModule } from './animal/animal.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql'
    }),
    UserModule,
    PrismaModule,
    AnimalModule
  ],
  providers: [AppService],
})
export class AppModule {}
