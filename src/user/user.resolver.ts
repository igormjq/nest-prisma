import { Resolver, Query, Args, Info } from '@nestjs/graphql';
import { User } from './user.type';
import { PrismaService } from 'src/prisma/prisma.service';

@Resolver()
export class UserResolver {
  constructor(private readonly prisma: PrismaService) {}

  @Query(returns => [User])
  async users(@Info() info): Promise<User[]> {
    return this.prisma.query.users(null, info);
  }
}
