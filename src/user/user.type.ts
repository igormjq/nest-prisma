import { ObjectType, Field, ID } from 'type-graphql'
import { Animal } from '../animal/animal.type';

@ObjectType()
export class User {
  
  @Field(type => ID)
  id: string;

  @Field()
  email: string;

  @Field()
  name: string;

  @Field(type => [Animal])
  animals: Animal[];
}