import { ObjectType, Field, ID } from 'type-graphql'
import { User } from '..//user/user.type';

@ObjectType()
export class Animal {
  
  @Field(type => ID)
  id: string;

  @Field()
  name: string;

  @Field()
  type: string;

  @Field(type => User)
  owner: User;
}