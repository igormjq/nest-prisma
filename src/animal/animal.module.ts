import { Module } from '@nestjs/common';
import { AnimalResolver } from './animal.resolver';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  providers: [AnimalResolver],
  imports: [PrismaModule],
})
export class AnimalModule {}
