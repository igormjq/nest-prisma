import { Resolver, Query, Info } from '@nestjs/graphql';
import { Animal } from './animal.type';
import { PrismaService } from '../prisma/prisma.service';

@Resolver('Animal')
export class AnimalResolver {
  constructor(
    private readonly prisma: PrismaService
  ){}

  @Query(returns => [Animal])
  async animals(@Info() info): Promise<Animal> {
    return this.prisma.query.animals(info);
  }
}
