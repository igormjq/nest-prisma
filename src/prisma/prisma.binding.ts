import { GraphQLResolveInfo, GraphQLSchema } from 'graphql'
import { IResolvers } from 'graphql-tools/dist/Interfaces'
import { Options } from 'graphql-binding'
import { makePrismaBindingClass, BasePrismaOptions } from 'prisma-binding'

export interface Query {
    users: <T = Array<User | null>>(args: { where?: UserWhereInput | null, orderBy?: UserOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    animals: <T = Array<Animal | null>>(args: { where?: AnimalWhereInput | null, orderBy?: AnimalOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    user: <T = User | null>(args: { where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    animal: <T = Animal | null>(args: { where: AnimalWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    usersConnection: <T = UserConnection>(args: { where?: UserWhereInput | null, orderBy?: UserOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    animalsConnection: <T = AnimalConnection>(args: { where?: AnimalWhereInput | null, orderBy?: AnimalOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    node: <T = Node | null>(args: { id: ID_Output }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> 
  }

export interface Mutation {
    createUser: <T = User>(args: { data: UserCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createAnimal: <T = Animal>(args: { data: AnimalCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateUser: <T = User | null>(args: { data: UserUpdateInput, where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateAnimal: <T = Animal | null>(args: { data: AnimalUpdateInput, where: AnimalWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteUser: <T = User | null>(args: { where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteAnimal: <T = Animal | null>(args: { where: AnimalWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    upsertUser: <T = User>(args: { where: UserWhereUniqueInput, create: UserCreateInput, update: UserUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertAnimal: <T = Animal>(args: { where: AnimalWhereUniqueInput, create: AnimalCreateInput, update: AnimalUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyUsers: <T = BatchPayload>(args: { data: UserUpdateManyMutationInput, where?: UserWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyAnimals: <T = BatchPayload>(args: { data: AnimalUpdateManyMutationInput, where?: AnimalWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyUsers: <T = BatchPayload>(args: { where?: UserWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyAnimals: <T = BatchPayload>(args: { where?: AnimalWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    executeRaw: <T = Json>(args: { database?: PrismaDatabase | null, query: String }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> 
  }

export interface Subscription {
    user: <T = UserSubscriptionPayload | null>(args: { where?: UserSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    animal: <T = AnimalSubscriptionPayload | null>(args: { where?: AnimalSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> 
  }

export interface Exists {
  User: (where?: UserWhereInput) => Promise<boolean>
  Animal: (where?: AnimalWhereInput) => Promise<boolean>
}

export interface Prisma {
  query: Query
  mutation: Mutation
  subscription: Subscription
  exists: Exists
  request: <T = any>(query: string, variables?: {[key: string]: any}) => Promise<T>
  delegate(operation: 'query' | 'mutation', fieldName: string, args: {
    [key: string]: any;
}, infoOrQuery?: GraphQLResolveInfo | string, options?: Options): Promise<any>;
delegateSubscription(fieldName: string, args?: {
    [key: string]: any;
}, infoOrQuery?: GraphQLResolveInfo | string, options?: Options): Promise<AsyncIterator<any>>;
getAbstractResolvers(filterSchema?: GraphQLSchema | string): IResolvers;
}

export interface BindingConstructor<T> {
  new(options: BasePrismaOptions): T
}
/**
 * Type Defs
*/

const typeDefs = `type AggregateAnimal {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type Animal implements Node {
  id: ID!
  name: String!
  type: String!
  owner: User!
}

"""A connection to a list of items."""
type AnimalConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [AnimalEdge]!
  aggregate: AggregateAnimal!
}

input AnimalCreateInput {
  id: ID
  name: String!
  type: String!
  owner: UserCreateOneWithoutAnimalsInput!
}

input AnimalCreateManyWithoutOwnerInput {
  create: [AnimalCreateWithoutOwnerInput!]
  connect: [AnimalWhereUniqueInput!]
}

input AnimalCreateWithoutOwnerInput {
  id: ID
  name: String!
  type: String!
}

"""An edge in a connection."""
type AnimalEdge {
  """The item at the end of the edge."""
  node: Animal!

  """A cursor for use in pagination."""
  cursor: String!
}

enum AnimalOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  type_ASC
  type_DESC
}

type AnimalPreviousValues {
  id: ID!
  name: String!
  type: String!
}

input AnimalScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [AnimalScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [AnimalScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AnimalScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  type: String

  """All values that are not equal to given value."""
  type_not: String

  """All values that are contained in given list."""
  type_in: [String!]

  """All values that are not contained in given list."""
  type_not_in: [String!]

  """All values less than the given value."""
  type_lt: String

  """All values less than or equal the given value."""
  type_lte: String

  """All values greater than the given value."""
  type_gt: String

  """All values greater than or equal the given value."""
  type_gte: String

  """All values containing the given string."""
  type_contains: String

  """All values not containing the given string."""
  type_not_contains: String

  """All values starting with the given string."""
  type_starts_with: String

  """All values not starting with the given string."""
  type_not_starts_with: String

  """All values ending with the given string."""
  type_ends_with: String

  """All values not ending with the given string."""
  type_not_ends_with: String
}

type AnimalSubscriptionPayload {
  mutation: MutationType!
  node: Animal
  updatedFields: [String!]
  previousValues: AnimalPreviousValues
}

input AnimalSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [AnimalSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [AnimalSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AnimalSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: AnimalWhereInput
}

input AnimalUpdateInput {
  name: String
  type: String
  owner: UserUpdateOneRequiredWithoutAnimalsInput
}

input AnimalUpdateManyDataInput {
  name: String
  type: String
}

input AnimalUpdateManyMutationInput {
  name: String
  type: String
}

input AnimalUpdateManyWithoutOwnerInput {
  create: [AnimalCreateWithoutOwnerInput!]
  connect: [AnimalWhereUniqueInput!]
  set: [AnimalWhereUniqueInput!]
  disconnect: [AnimalWhereUniqueInput!]
  delete: [AnimalWhereUniqueInput!]
  update: [AnimalUpdateWithWhereUniqueWithoutOwnerInput!]
  updateMany: [AnimalUpdateManyWithWhereNestedInput!]
  deleteMany: [AnimalScalarWhereInput!]
  upsert: [AnimalUpsertWithWhereUniqueWithoutOwnerInput!]
}

input AnimalUpdateManyWithWhereNestedInput {
  where: AnimalScalarWhereInput!
  data: AnimalUpdateManyDataInput!
}

input AnimalUpdateWithoutOwnerDataInput {
  name: String
  type: String
}

input AnimalUpdateWithWhereUniqueWithoutOwnerInput {
  where: AnimalWhereUniqueInput!
  data: AnimalUpdateWithoutOwnerDataInput!
}

input AnimalUpsertWithWhereUniqueWithoutOwnerInput {
  where: AnimalWhereUniqueInput!
  update: AnimalUpdateWithoutOwnerDataInput!
  create: AnimalCreateWithoutOwnerInput!
}

input AnimalWhereInput {
  """Logical AND on all given filters."""
  AND: [AnimalWhereInput!]

  """Logical OR on all given filters."""
  OR: [AnimalWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AnimalWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  type: String

  """All values that are not equal to given value."""
  type_not: String

  """All values that are contained in given list."""
  type_in: [String!]

  """All values that are not contained in given list."""
  type_not_in: [String!]

  """All values less than the given value."""
  type_lt: String

  """All values less than or equal the given value."""
  type_lte: String

  """All values greater than the given value."""
  type_gt: String

  """All values greater than or equal the given value."""
  type_gte: String

  """All values containing the given string."""
  type_contains: String

  """All values not containing the given string."""
  type_not_contains: String

  """All values starting with the given string."""
  type_starts_with: String

  """All values not starting with the given string."""
  type_not_starts_with: String

  """All values ending with the given string."""
  type_ends_with: String

  """All values not ending with the given string."""
  type_not_ends_with: String
  owner: UserWhereInput
}

input AnimalWhereUniqueInput {
  id: ID
}

type BatchPayload {
  """The number of nodes that have been affected by the Batch operation."""
  count: Long!
}

"""Raw JSON value"""
scalar Json

"""
The \`Long\` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
"""
scalar Long

type Mutation {
  createUser(data: UserCreateInput!): User!
  createAnimal(data: AnimalCreateInput!): Animal!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateAnimal(data: AnimalUpdateInput!, where: AnimalWhereUniqueInput!): Animal
  deleteUser(where: UserWhereUniqueInput!): User
  deleteAnimal(where: AnimalWhereUniqueInput!): Animal
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  upsertAnimal(where: AnimalWhereUniqueInput!, create: AnimalCreateInput!, update: AnimalUpdateInput!): Animal!
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  updateManyAnimals(data: AnimalUpdateManyMutationInput!, where: AnimalWhereInput): BatchPayload!
  deleteManyUsers(where: UserWhereInput): BatchPayload!
  deleteManyAnimals(where: AnimalWhereInput): BatchPayload!
  executeRaw(database: PrismaDatabase, query: String!): Json!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: String

  """When paginating forwards, the cursor to continue."""
  endCursor: String
}

enum PrismaDatabase {
  default
}

type Query {
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  animals(where: AnimalWhereInput, orderBy: AnimalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Animal]!
  user(where: UserWhereUniqueInput!): User
  animal(where: AnimalWhereUniqueInput!): Animal
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  animalsConnection(where: AnimalWhereInput, orderBy: AnimalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): AnimalConnection!

  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
}

type Subscription {
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
  animal(where: AnimalSubscriptionWhereInput): AnimalSubscriptionPayload
}

type User implements Node {
  id: ID!
  name: String!
  email: String!
  animals(where: AnimalWhereInput, orderBy: AnimalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Animal!]
}

"""A connection to a list of items."""
type UserConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  id: ID
  name: String!
  email: String!
  animals: AnimalCreateManyWithoutOwnerInput
}

input UserCreateOneWithoutAnimalsInput {
  create: UserCreateWithoutAnimalsInput
  connect: UserWhereUniqueInput
}

input UserCreateWithoutAnimalsInput {
  id: ID
  name: String!
  email: String!
}

"""An edge in a connection."""
type UserEdge {
  """The item at the end of the edge."""
  node: User!

  """A cursor for use in pagination."""
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  email_ASC
  email_DESC
}

type UserPreviousValues {
  id: ID!
  name: String!
  email: String!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [UserSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: UserWhereInput
}

input UserUpdateInput {
  name: String
  email: String
  animals: AnimalUpdateManyWithoutOwnerInput
}

input UserUpdateManyMutationInput {
  name: String
  email: String
}

input UserUpdateOneRequiredWithoutAnimalsInput {
  create: UserCreateWithoutAnimalsInput
  connect: UserWhereUniqueInput
  update: UserUpdateWithoutAnimalsDataInput
  upsert: UserUpsertWithoutAnimalsInput
}

input UserUpdateWithoutAnimalsDataInput {
  name: String
  email: String
}

input UserUpsertWithoutAnimalsInput {
  update: UserUpdateWithoutAnimalsDataInput!
  create: UserCreateWithoutAnimalsInput!
}

input UserWhereInput {
  """Logical AND on all given filters."""
  AND: [UserWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  animals_every: AnimalWhereInput
  animals_some: AnimalWhereInput
  animals_none: AnimalWhereInput
}

input UserWhereUniqueInput {
  id: ID
}
`

export const Prisma = makePrismaBindingClass<BindingConstructor<Prisma>>({typeDefs})

/**
 * Types
*/

export type AnimalOrderByInput =   'id_ASC' |
  'id_DESC' |
  'name_ASC' |
  'name_DESC' |
  'type_ASC' |
  'type_DESC'

export type MutationType =   'CREATED' |
  'UPDATED' |
  'DELETED'

export type PrismaDatabase =   'default'

export type UserOrderByInput =   'id_ASC' |
  'id_DESC' |
  'name_ASC' |
  'name_DESC' |
  'email_ASC' |
  'email_DESC'

export interface AnimalCreateInput {
  id?: ID_Input | null
  name: String
  type: String
  owner: UserCreateOneWithoutAnimalsInput
}

export interface AnimalCreateManyWithoutOwnerInput {
  create?: AnimalCreateWithoutOwnerInput[] | AnimalCreateWithoutOwnerInput | null
  connect?: AnimalWhereUniqueInput[] | AnimalWhereUniqueInput | null
}

export interface AnimalCreateWithoutOwnerInput {
  id?: ID_Input | null
  name: String
  type: String
}

export interface AnimalScalarWhereInput {
  AND?: AnimalScalarWhereInput[] | AnimalScalarWhereInput | null
  OR?: AnimalScalarWhereInput[] | AnimalScalarWhereInput | null
  NOT?: AnimalScalarWhereInput[] | AnimalScalarWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  type?: String | null
  type_not?: String | null
  type_in?: String[] | String | null
  type_not_in?: String[] | String | null
  type_lt?: String | null
  type_lte?: String | null
  type_gt?: String | null
  type_gte?: String | null
  type_contains?: String | null
  type_not_contains?: String | null
  type_starts_with?: String | null
  type_not_starts_with?: String | null
  type_ends_with?: String | null
  type_not_ends_with?: String | null
}

export interface AnimalSubscriptionWhereInput {
  AND?: AnimalSubscriptionWhereInput[] | AnimalSubscriptionWhereInput | null
  OR?: AnimalSubscriptionWhereInput[] | AnimalSubscriptionWhereInput | null
  NOT?: AnimalSubscriptionWhereInput[] | AnimalSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: AnimalWhereInput | null
}

export interface AnimalUpdateInput {
  name?: String | null
  type?: String | null
  owner?: UserUpdateOneRequiredWithoutAnimalsInput | null
}

export interface AnimalUpdateManyDataInput {
  name?: String | null
  type?: String | null
}

export interface AnimalUpdateManyMutationInput {
  name?: String | null
  type?: String | null
}

export interface AnimalUpdateManyWithoutOwnerInput {
  create?: AnimalCreateWithoutOwnerInput[] | AnimalCreateWithoutOwnerInput | null
  connect?: AnimalWhereUniqueInput[] | AnimalWhereUniqueInput | null
  set?: AnimalWhereUniqueInput[] | AnimalWhereUniqueInput | null
  disconnect?: AnimalWhereUniqueInput[] | AnimalWhereUniqueInput | null
  delete?: AnimalWhereUniqueInput[] | AnimalWhereUniqueInput | null
  update?: AnimalUpdateWithWhereUniqueWithoutOwnerInput[] | AnimalUpdateWithWhereUniqueWithoutOwnerInput | null
  updateMany?: AnimalUpdateManyWithWhereNestedInput[] | AnimalUpdateManyWithWhereNestedInput | null
  deleteMany?: AnimalScalarWhereInput[] | AnimalScalarWhereInput | null
  upsert?: AnimalUpsertWithWhereUniqueWithoutOwnerInput[] | AnimalUpsertWithWhereUniqueWithoutOwnerInput | null
}

export interface AnimalUpdateManyWithWhereNestedInput {
  where: AnimalScalarWhereInput
  data: AnimalUpdateManyDataInput
}

export interface AnimalUpdateWithoutOwnerDataInput {
  name?: String | null
  type?: String | null
}

export interface AnimalUpdateWithWhereUniqueWithoutOwnerInput {
  where: AnimalWhereUniqueInput
  data: AnimalUpdateWithoutOwnerDataInput
}

export interface AnimalUpsertWithWhereUniqueWithoutOwnerInput {
  where: AnimalWhereUniqueInput
  update: AnimalUpdateWithoutOwnerDataInput
  create: AnimalCreateWithoutOwnerInput
}

export interface AnimalWhereInput {
  AND?: AnimalWhereInput[] | AnimalWhereInput | null
  OR?: AnimalWhereInput[] | AnimalWhereInput | null
  NOT?: AnimalWhereInput[] | AnimalWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  type?: String | null
  type_not?: String | null
  type_in?: String[] | String | null
  type_not_in?: String[] | String | null
  type_lt?: String | null
  type_lte?: String | null
  type_gt?: String | null
  type_gte?: String | null
  type_contains?: String | null
  type_not_contains?: String | null
  type_starts_with?: String | null
  type_not_starts_with?: String | null
  type_ends_with?: String | null
  type_not_ends_with?: String | null
  owner?: UserWhereInput | null
}

export interface AnimalWhereUniqueInput {
  id?: ID_Input | null
}

export interface UserCreateInput {
  id?: ID_Input | null
  name: String
  email: String
  animals?: AnimalCreateManyWithoutOwnerInput | null
}

export interface UserCreateOneWithoutAnimalsInput {
  create?: UserCreateWithoutAnimalsInput | null
  connect?: UserWhereUniqueInput | null
}

export interface UserCreateWithoutAnimalsInput {
  id?: ID_Input | null
  name: String
  email: String
}

export interface UserSubscriptionWhereInput {
  AND?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  OR?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  NOT?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: UserWhereInput | null
}

export interface UserUpdateInput {
  name?: String | null
  email?: String | null
  animals?: AnimalUpdateManyWithoutOwnerInput | null
}

export interface UserUpdateManyMutationInput {
  name?: String | null
  email?: String | null
}

export interface UserUpdateOneRequiredWithoutAnimalsInput {
  create?: UserCreateWithoutAnimalsInput | null
  connect?: UserWhereUniqueInput | null
  update?: UserUpdateWithoutAnimalsDataInput | null
  upsert?: UserUpsertWithoutAnimalsInput | null
}

export interface UserUpdateWithoutAnimalsDataInput {
  name?: String | null
  email?: String | null
}

export interface UserUpsertWithoutAnimalsInput {
  update: UserUpdateWithoutAnimalsDataInput
  create: UserCreateWithoutAnimalsInput
}

export interface UserWhereInput {
  AND?: UserWhereInput[] | UserWhereInput | null
  OR?: UserWhereInput[] | UserWhereInput | null
  NOT?: UserWhereInput[] | UserWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  email?: String | null
  email_not?: String | null
  email_in?: String[] | String | null
  email_not_in?: String[] | String | null
  email_lt?: String | null
  email_lte?: String | null
  email_gt?: String | null
  email_gte?: String | null
  email_contains?: String | null
  email_not_contains?: String | null
  email_starts_with?: String | null
  email_not_starts_with?: String | null
  email_ends_with?: String | null
  email_not_ends_with?: String | null
  animals_every?: AnimalWhereInput | null
  animals_some?: AnimalWhereInput | null
  animals_none?: AnimalWhereInput | null
}

export interface UserWhereUniqueInput {
  id?: ID_Input | null
}

/*
 * An object with an ID

 */
export interface Node {
  id: ID_Output
}

export interface AggregateAnimal {
  count: Int
}

export interface AggregateUser {
  count: Int
}

export interface Animal extends Node {
  id: ID_Output
  name: String
  type: String
  owner: User
}

/*
 * A connection to a list of items.

 */
export interface AnimalConnection {
  pageInfo: PageInfo
  edges: Array<AnimalEdge | null>
  aggregate: AggregateAnimal
}

/*
 * An edge in a connection.

 */
export interface AnimalEdge {
  node: Animal
  cursor: String
}

export interface AnimalPreviousValues {
  id: ID_Output
  name: String
  type: String
}

export interface AnimalSubscriptionPayload {
  mutation: MutationType
  node?: Animal | null
  updatedFields?: Array<String> | null
  previousValues?: AnimalPreviousValues | null
}

export interface BatchPayload {
  count: Long
}

/*
 * Information about pagination in a connection.

 */
export interface PageInfo {
  hasNextPage: Boolean
  hasPreviousPage: Boolean
  startCursor?: String | null
  endCursor?: String | null
}

export interface User extends Node {
  id: ID_Output
  name: String
  email: String
  animals?: Array<Animal> | null
}

/*
 * A connection to a list of items.

 */
export interface UserConnection {
  pageInfo: PageInfo
  edges: Array<UserEdge | null>
  aggregate: AggregateUser
}

/*
 * An edge in a connection.

 */
export interface UserEdge {
  node: User
  cursor: String
}

export interface UserPreviousValues {
  id: ID_Output
  name: String
  email: String
}

export interface UserSubscriptionPayload {
  mutation: MutationType
  node?: User | null
  updatedFields?: Array<String> | null
  previousValues?: UserPreviousValues | null
}

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number
export type ID_Output = string

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.
*/
export type Int = number

/*
Raw JSON value
*/
export type Json = any

/*
The `Long` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
*/
export type Long = string

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string